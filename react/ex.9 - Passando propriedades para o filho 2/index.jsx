import React from 'react'
import ReactDOM from 'react-dom'
import Family from './family'
import Member from './member'

ReactDOM.render(
	<Family lastName="Custodio">
		<Member name="Igor"></Member>
		<Member name="Rose"></Member>
		<Member name="Antonio"></Member>
	</Family>,
	document.getElementById('app')
)
