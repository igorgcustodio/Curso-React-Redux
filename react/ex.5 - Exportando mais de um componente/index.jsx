import React from 'react'
import ReactDOM from 'react-dom'
import Primeiro, { Segundo } from './component'

ReactDOM.render(
	<div>
		<Primeiro value="oi"/>
		<Segundo value="tudo bem?"/>
	</div>,
	document.getElementById('app')
)
