import React from 'react'

export default props => (
	<h1>1 - {props.value}</h1>
)

export const Segundo = props => (
	<h1>2 - {props.value}</h1>
)

// export { Primeiro, Segundo }