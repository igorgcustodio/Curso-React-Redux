# Repositório para o curso de React e Redux

[Link do Curso no Udemy](https://www.udemy.com/react-redux-pt)

- [x] Webpack 
- [x] React
- [x] Todo-app
  - [x] Back-end
  - [x] Front-end
- [ ] React com Redux
- [ ] Migração do app para Redux
- [ ] Aplicação final (Ciclo de Pagamentos)
  - [ ] Back-end
  - [ ] Front-end
- [ ] Aplicação final (Autenticação)
  - [ ] Back-end
  - [ ] Front-end
- [ ] Extras


# Dicas úteis

- Para ver o arquivo do bundle.js, basta digitar no terminal
  - ``` ./node_modules/.bin/webpack ```
